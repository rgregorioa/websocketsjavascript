package com.byteslounge.websockets;

import com.byteslounge.dbl.DoublyLinkedList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import com.byteslounge.players.Player;
import com.byteslounge.spl.LinkedList;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocket")
public class WebSocket {

    // cartas ordenadas
    private static ArrayList<String> temp = new ArrayList<>(Arrays.asList(
            "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9",
            "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9",
            "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
            "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9"));
    // lista simple general que se utilizara como una pila
    private static LinkedList list = new LinkedList();
    // llevar el control de sessiones
    private static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());
    // Hash set de sessiones
    private static DoublyLinkedList list1 = new DoublyLinkedList();
    // lista doblemente enlazada de carta del jugador 1
    private static DoublyLinkedList list2 = new DoublyLinkedList();
    // lista doblemente enlazada de carta del jugador 2
    private static DoublyLinkedList list3 = new DoublyLinkedList();
    // lista doblemente enlazada de carta del jugador 3
    private static ArrayList<Player> players = new ArrayList<>();
    private static String turn = "1";

    // genera numeros random para generar un nuevo orden en cada juego
    public void fillCards() {
        while (!temp.isEmpty()) {
            byte rand = (byte) (Math.random() * (temp.size()));
            list.add(temp.remove(rand));
        }
        //list.show();
    }

    // se ejecuta cada vez que un nuevo jugador inicia y al presionar el boton movement
    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException {
        synchronized (clients) {
            System.out.println(message);
            // este for es para hacer lo mismo en las sesiones activas
            for (Session client : clients) {
                // envia este String separado por comas al java script en cada sesion activa
                client.getBasicRemote().sendText(
                        // mensaje a mostrar de cantidad de jugadores conectados
                        showPlayersConected(clients.size()) + ","
                        + //tamaño de array de jugadores conectados
                        clients.size() + ","
                        // tamaño lista1        
                        + list1.getSize() + ","
                        + // tamaño lista2
                        list2.getSize() + ","
                        + // tamaño lista3        
                        list3.getSize() + ","
                        + // numero de carta        
                        message);
                //}
            }
        }

    }

    // unicamente retorna la cantidad de jugadores conectados
    public String showPlayersConected(int i) {
        String message;
        switch (i) {
            case 1:
                message = "Un jugador conectado";
                break;
            case 2:
                message = "Dos jugadores conectados";
                break;
            case 3:
                message = "Tres jugadores conectados";
                break;
            default:
                message = "...";
                break;
        }
        return message;
    }

    
    private void distribution(Player player) {
        int size;
        if (list1.isEmpty() && list2.isEmpty() && list3.isEmpty()) {
            size = 13;
            //System.out.println(size + "    " + list.getSize() + "    " + list1.getSize()  + "    " + list2.getSize()  + "    " + list3.getSize());
            for (byte i = 0; i < size; i++) {
                list1.addLeft(list.delete().getData().toString());
            }
            player.setCards(list1);
            return;
        } else if (!list1.isEmpty() && list2.isEmpty() && list3.isEmpty()) {
            size = 13;
            for (byte i = 0; i < size; i++) {
                list2.addLeft(list.delete().getData().toString());
            }
            player.setCards(list2);
            return;
        } else if (!list1.isEmpty() && !list2.isEmpty() && list3.isEmpty()) {
            size = 14;
            for (byte i = 0; i < size; i++) {
                list3.addLeft(list.delete().getData().toString());
            }
            player.setCards(list3);
            return;
        } else{
            // falta regresar cartas del mazo al jugador que perdio
        }
    }

    // se ejecuta unicamente cuando un usuario ingresa a la url del proyecto y crea una nueva session
    @OnOpen
    public void onOpen(Session session) throws IOException {
        // este if es para que cuando se conecte un nuevo usuario y ya hay 3 conectados, no haga nada
        if (clients.size() != 3) {
            // agrega la session al hash set
            clients.add(session);
            // se instancia un nuevo objeto tipo player
            Player player = new Player(session.getId(), session);
            // se revuelven las cartas del mazo solo cuando inicia session el primer jugador
            if (clients.size() == 1) {
                fillCards();
            }
            // distribuye las cartas correspondientes a cada jugador
            distribution(player);
            // agrega el objeto jugador al ArrayList
            players.add(player);
            synchronized (clients) {
                // muestra el mensaje de usuarios conectados y estadisticas de cartas por jugador
                for (Session client : clients) {
                    client.getBasicRemote().sendText(showPlayersConected(clients.size()) + "," + clients.size() + ","
                            + list1.getSize() + "," + list2.getSize() + "," + list3.getSize() + "," + "");
                }
            }
            //System.out.println(list.getSize() + "    " + list1.getSize() + "    " + list2.getSize() + "    " + list3.getSize());
        }
    }

    // cierra la session por jugador que sale del juego
    @OnClose
    public void onClose(Session session) throws IOException {
        // Remove session from the connected sessions set
        for (Player pl : players) {
            // aun esta pendiente regresar cartas al mazo general
            if (session == pl.getpSession()) {

                System.out.println("**");
            }
        }
        // remueve sesion del hash set
        clients.remove(session);
        synchronized (clients) {
            for (Session client : clients) {
                client.getBasicRemote().sendText("Juego finalizado");
            }
        }
    }

}
