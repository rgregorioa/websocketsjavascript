package com.byteslounge.spl;

import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fx62
 */
public class LinkedList {
    private Node first;
    private Node last;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public Node getLast() {
        return last;
    }
    
    public Node getFirst() {
        return first;
    }

    public LinkedList(){
        first = null;
        last = null;
    }

    public boolean isEmpty(){
        return first == null;
    }
    
    public void add(Object data){
        Node node = new Node(data);
        if(!isEmpty()){
            node.setNext(first);
        } else {
            last = node;
        }
        first = node;
        size++;
    }
    
    /*public void addFirst(Object data){
        if(isEmpty()){
            add(data);
        } else{
            Node nuevo = new Node(data);
            last.setSig(nuevo);
            last = nuevo;
            // int a = 2;;
        }
    }*/
    
    /*public void deleteLast(){
        if(first == last && first!= null){
            first=last=null;
        }
        if(!isEmpty()){
            last = devolverPenlast();
            last.setSig(null);
        }
    }*/
    
    /*public String showRecursivity(Node nodo){
        if(nodo.getSig() == null){
            return nodo.getDato() + " -> null";
        } else{
            return nodo.getDato() + " -> " + showRecursivity(nodo.getSig());
        }
    }*/
    
    /*public Node devolverPenlast(){
        Node temp = first;
        while(temp.getSig() != last){
            temp=temp.getSig();
        }
        return temp;
    }*/

    public void show(){
        Node temp = first;
        while(temp != null){
            System.out.print(temp.getData() + " -> ");
            temp=temp.getNext();
        }
        System.out.println();
    }

    /*public static void main(String[] args){
        LinkedList lista = new LinkedList();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.show();
        lista.delete();
        lista.show();
        lista.delete();
        lista.show();
        lista.delete();
        lista.show();
    }*/

    public Node delete(){
        Node deleted = null;
        if(!isEmpty()){
            deleted = first;
            first = first.getNext();
        }
        if (first == null){
            last = first;
        }
        return deleted;
    }
}
