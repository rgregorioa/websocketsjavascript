// abrir el websocket, al ejecutarlo en red es necesario cambiar localhost por la ip del equipo donde se ejecute el proyecto
const webSocket = new WebSocket('ws://localhost:8080/byteslounge/websocket');
var player;

// unicamente se ejecuta por al fallo o error inesperado
webSocket.onerror = function (event) {
    onError(event);
};

// unicamente se ejecuta una vez al abrir la pagina por cada usuario
webSocket.onopen = function (event) {
    //onOpen es otra funcion que lleva la logica de nueva sesion para comunicarse con java
    onOpen(event);
};

// se ejecuta cada vez que se presione el boton de movement y tambien se debe de ejecutar al presionar el boton mazo
webSocket.onmessage = function (event) {
    // onMessage se ejecuta multiples veces por sesion
    onMessage(event);
};

// esta funcion es la principal y que lleva la mayor logica del juego
function onMessage(event) {
    // csv recibe un string separado por comas por parte de java, se parsean las comas y se guardan los valores en un array de javascript
    const csv = event.data.split(',');
    // para mostrar en consola los datos que envia java
    console.log(csv);
    // muestra la cantidad de jugadores conectados en la esquina superior izquierda
    document.getElementById('messages').innerHTML
            = csv[0];
    // agrega la informacion de cada jugador conectado
    switch(csv[1]){
        case '1':
            document.querySelector('#area1').innerHTML = 'Jugador 1<hr>' + csv[2] + ' cartas disponibles';
            break;
        case '2':
            document.querySelector('#area1').innerHTML = 'Jugador 1<hr>' + csv[2] + ' cartas disponibles';
            document.querySelector('#area2').innerHTML = 'Jugador 2<hr>' + csv[3] + ' cartas disponibles';
            break;
        case '3':
            document.querySelector('#area1').innerHTML = 'Jugador 1<hr>' + csv[2] + ' cartas disponibles';
            document.querySelector('#area2').innerHTML = 'Jugador 2<hr>' + csv[3] + ' cartas disponibles';
            document.querySelector('#area3').innerHTML = 'Jugador 3<hr>' + csv[4] + ' cartas disponibles';
            break;
    } 
    // segun el numero de carta dibuja en el centro de la pagina una carta con canvas de html5
    let card = csv[5];
    let bg = '';
    let font = '';
    switch(card.charAt(0)){
        case 'A':
            bg = '#8B0000';
            font = '#ffff00';
            break;
        case 'B':
            bg = '#008000';
            font = '#FFD700';
            break;
        case 'C':
            bg = '#191970';
            font = '#B0E0E6';
            break;
        case 'D':
            bg = '#FFA500';
            font = '#FF4500';
            break;
        default:
            break;
    }
    const area = document.querySelector('#area');
    const areaContext = area.getContext("2d");
    areaContext.font = "bold 50px TimesNewRomans";
    areaContext.fillStyle = bg;
    areaContext.fillRect(0, 0, area.width, area.height);
    areaContext.fillStyle = font;
    areaContext.fillText(card.charAt(1), (area.width / 2) - 10, (area.height / 2) + 10);
    // red => bg = #8B0000 fn = #ffff00
    // green => bg = #008000 fn = #FFD700
    // blue => bg = #191970 fn = #B0E0E6
    // orange => bg = #FFA500 fn = #FF4500
}

// se ejecuta una sola vez por session
function onOpen(event) {
    //player = prompt("Please select your nickname, be careful champion\n\Because this name will you represent all the journey", ""); 
    //send();
    //if (player) {
        //document.getElementById('messages').innerHTML = player;
    //}
}

// se ejecuta solo en caso de algun fallo
function onError(event) {
    alert(event.data);
}

// con esta funcion se envian informacion de java script a java
function send() {
    var txt = document.getElementById('nickname').value;
    webSocket.send(txt);
    return false;
}